/*
  #     #  ####  #    # ###### #      # #    # ######
  #     # #    # ##  ## #      #      # #   #  #
  ####### #    # # ## # #####  #      # ####   #####
  #     # #    # #    # #      #      # #  #   #
  #     #  ####  #    # ###### ###### # #    # ######

  Name:-> Signin Component
  Description:-> This is used to render the dashboard of the user.
  Author:-> Kajal
  Created On:-> 18-08-2017
-----------------------------------------------------------------------------*/
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

class ProtectedContent extends Component {
  componentWillMount() {
    this.props.fetchMessage();
  }

  render() {
    return (
      // <div className="msg">{this.props.message}</div>
      <div className="msg"></div>
      //user dashboard


    );
  }
}

function mapStateToProps(state) {
  return { message: state.auth.message };
}

export default connect(mapStateToProps, actions)(ProtectedContent);
