/*
  #     #  ####  #    # ###### #      # #    # ######
  #     # #    # ##  ## #      #      # #   #  #
  ####### #    # # ## # #####  #      # ####   #####
  #     # #    # #    # #      #      # #  #   #
  #     #  ####  #    # ###### ###### # #    # ######

  Name:-> Auth Reducer
  Description:-> This reducer used to set the state of the application.
  Author:-> Kajal
  Created On:-> 18-08-2017
-----------------------------------------------------------------------------*/
import {
  AUTH_USER,
  UNAUTH_USER,
  AUTH_ERROR,
  FETCH_MESSAGE,
  FETCH_USER
} from '../actions/types';


export default function(state = { authenticated: false, admin_privileges: false }, action) {

  switch(action.type) {
    case AUTH_USER:
      return { ...state, error: '', authenticated: true };
    case UNAUTH_USER:
      return { ...state, authenticated: false, admin_privileges: false };
    case AUTH_ERROR:
      return { ...state, error: action.payload };
    case FETCH_MESSAGE:
      return { ...state, message: action.payload };
    case FETCH_USER:
      return { ...state, message: action.payload };
  }

  return state;

}
