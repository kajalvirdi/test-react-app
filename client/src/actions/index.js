/*
  #     #  ####  #    # ###### #      # #    # ######
  #     # #    # ##  ## #      #      # #   #  #
  ####### #    # # ## # #####  #      # ####   #####
  #     # #    # #    # #      #      # #  #   #
  #     #  ####  #    # ###### ###### # #    # ######

  Name:-> Index Action
  Description:-> This is used to manage the requests to the server APIs.
  Author:-> Kajal
  Created On:-> 18-08-2017
-----------------------------------------------------------------------------*/
import axios from 'axios';
import { browserHistory } from 'react-router';
import {
  AUTH_USER,
  UNAUTH_USER,
  AUTH_ERROR,
  FETCH_MESSAGE,
  FETCH_USER
} from './types';

const jwt_decode = require('jwt-decode');

//server address
const ROOT_URL = 'http://localhost:3090';

/*-----------------authenticate user function ------------------------------*/
export function signinUser({ email, password }) {
  return function(dispatch) {
    // Submit email/password to the server
    axios.post(`${ROOT_URL}/signin`, { email, password })
      .then(response => {
        // If request is good...
        // - Update state to indicate user is authenticated
        dispatch({ type: AUTH_USER });

        // decode token for info on the user
        let decoded_token_data = jwt_decode(response.data.token);

        // - Save the JWT token
        localStorage.setItem('token', response.data.token);

        // Save User ID
        localStorage.setItem('user_id', response.data.id);

        // - redirect to the appropriate route
        if(decoded_token_data.role == 'user') {
          browserHistory.push('/dashboard');
        }
        else {
          browserHistory.push('/');
        }


      })
      .catch(() => {
        // If request is bad...
        // - Show an error to the user
        dispatch(authError('Bad Login Info'));
      });
  }
}

/*-----------------register the user function -------------------------------*/
export function signupUser({ email, password }) {
  return function(dispatch) {
    axios.post(`${ROOT_URL}/signup`, { email, password })
      .then(response => {
        dispatch({ type: AUTH_USER });
        localStorage.setItem('token', response.data.token);
        browserHistory.push('/dashboard');
      })
      .catch(response => dispatch(authError(response.data.error)));
  }
}

/*-----------------to get auth error function---------------------------------*/
export function authError(error) {
  return {
    type: AUTH_ERROR,
    payload: error
  };
}

/*-----------------signout function -----------------------------------------*/
export function signoutUser() {
  localStorage.removeItem('token');
  return { type: UNAUTH_USER };
}


/*-----------------fetch message to show ------------------------------------*/
//token included in the header of the request for authorization
export function fetchMessage() {
  return function(dispatch) {
    axios.get(`${ROOT_URL}/dashboard`, {
      headers: { authorization: localStorage.getItem('token') }
    })
      .then(response => {
        dispatch({
          type: FETCH_MESSAGE,
          payload: response.data.message
        });
      });
  }
}


/*-----------------fetch user data to show on the profile page---------------*/
export function fetchUserData(){
  console.log('in fetch user')
  var id = localStorage.getItem('user_id');
  return (dispatch) => {
    axios.get(`${ROOT_URL}/get_user/`+ id +``,
      { headers: { authorization: localStorage.getItem('token') }
    })
      .then(response => {
        dispatch({
          type: FETCH_USER,
          payload: response.data.message
        });

        //this.setState({ message_user : response.data })
      });
    // return messageApi.newMessage(data).then((response) => {
    //   dispatch(saveMessage({room: data.room, message: response.data}))
    //   return response
    // }
  }

}
