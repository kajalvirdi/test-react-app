/*
  #     #  ####  #    # ###### #      # #    # ######
  #     # #    # ##  ## #      #      # #   #  #
  ####### #    # # ## # #####  #      # ####   #####
  #     # #    # #    # #      #      # #  #   #
  #     #  ####  #    # ###### ###### # #    # ######

  Name:-> Index JS
  Description:-> This is used to create our router.
  Author:-> Kajal
  Created On:-> 18-08-2017
-----------------------------------------------------------------------------*/
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import reduxThunk from 'redux-thunk';

import App from './components/app';
import Signin from './components/auth/signin';
import Signout from './components/auth/signout';
import Signup from './components/auth/signup';
import ProtectedContent from './components/protected_content';
import RequireAuth from './components/auth/require_auth';
import ShowWithoutAuth from './components/auth/show_without_auth';
import Default from './components/default';
import Profile from './components/profile';
import reducers from './reducers';
import { AUTH_USER } from './actions/types';
import { SET_ADMIN_PRIVILEGES } from './actions/types';
import jwt_decode from 'jwt-decode';


const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const store = createStoreWithMiddleware(reducers);

const token = localStorage.getItem('token');
// update application state with token information if needed
if (token) {
  // update authentication flag
  store.dispatch({ type: AUTH_USER });

}


ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={App}>
        <IndexRoute component={ShowWithoutAuth(Default)} />
        <Route path="signin" component={ShowWithoutAuth(Signin)} />
        <Route path="signup" component={ShowWithoutAuth(Signup)} />
        <Route path="signout" component={Signout} />

        <Route path="dashboard" component={RequireAuth(ProtectedContent)} />
        <Route path="profile" component={RequireAuth(Profile)} />
      </Route>
    </Router>
  </Provider>
  , document.querySelector('.container'));
