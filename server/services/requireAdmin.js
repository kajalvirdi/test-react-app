/*
  #     #  ####  #    # ###### #      # #    # ######
  #     # #    # ##  ## #      #      # #   #  #
  ####### #    # # ## # #####  #      # ####   #####
  #     # #    # #    # #      #      # #  #   #
  #     #  ####  #    # ###### ###### # #    # ######

  Name:-> Require Admin
  Description:-> This service is used  to autenticate the user with passport module
  Author:-> Kajal
  Created On:-> 18-08-2017
-----------------------------------------------------------------------------*/
module.exports = function (req, res, next) {
  if(req.user.role === 'admin'){
    next();
  }
  else {
    res.send({ message: 'Unauthorized!' });
  }

};
