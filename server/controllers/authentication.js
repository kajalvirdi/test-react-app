/*
  #     #  ####  #    # ###### #      # #    # ######
  #     # #    # ##  ## #      #      # #   #  #
  ####### #    # # ## # #####  #      # ####   #####
  #     # #    # #    # #      #      # #  #   #
  #     #  ####  #    # ###### ###### # #    # ######

  Name:-> Auth Controller
  Description:-> This controoler is used manage auth functionality
  Author:-> Kajal
  Created On:-> 18-08-2017
-----------------------------------------------------------------------------*/
const jwt = require('jwt-simple');
const User = require('../models/user');
const config = require('../config');
var ObjectId = require('mongodb').ObjectID;

function tokenForUser(user) {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user.id, iat: timestamp, role: user.role }, config.secret);
}

/*-------function to authenticate user---------------------------------------*/
exports.signin = function(req, res, next) {
  // User has been authenticated, send back token and user id
  res.send({ token: tokenForUser(req.user), id: req.user.id});
}

/*-------function to register the user---------------------------------------*/
exports.signup = function(req, res, next) {
  const email = req.body.email;
  const password = req.body.password;

  if (!email || !password) {
    return res.status(422).send({ error: 'You must provide email and password'});
  }

  // See if a user with the given email exists
  User.findOne({ email: email }, function(err, existingUser) {
    if (err) { return next(err); }

    // If a user with email does exist, return an error
    if (existingUser) {
      return res.status(422).send({ error: 'Email is in use' });
    }

    // If a user with email does NOT exist, create and save user record
    const user = new User({
      email: email,
      password: password,
      role: 'user'
    });

    user.save(function(err) {
      if (err) { return next(err); }

      // Repond to request indicating the user was created
      res.json({ token: tokenForUser(user) });
    });
  });
}

/*-------function to get user via id-----------------------------------------*/
exports.get_user = function(req, res, next) {

  const id = req.params.id;
  if (!id) {
    return res.status(422).send({ error: 'User id is not valid'});
  }

  // See if a user with the given id exists
  User.findOne({ "_id": new ObjectId(id) }, function(err, existingUser) {
    if (err) { return next(err); }

    // If a user with id exist, return user json
    if (existingUser) {
      res.send({"message": existingUser});
    }
  });
}
