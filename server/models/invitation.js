/*
  #     #  ####  #    # ###### #      # #    # ######
  #     # #    # ##  ## #      #      # #   #  #
  ####### #    # # ## # #####  #      # ####   #####
  #     # #    # #    # #      #      # #  #   #
  #     #  ####  #    # ###### ###### # #    # ######

  Name:-> Invitation Model
  Description:-> This model is used  to store invitations sent to user's friends
  Author:-> Kajal
  Created On:-> 19-08-2017
-----------------------------------------------------------------------------*/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// model definition
const invitationSchema = new Schema({
  user_from: String,
  user_to: String,
  sent_at: { type: Date, default: Date.now }
});

// Create the model class
const ModelClass = mongoose.model('invitation', invitationSchema);

// Export the model
module.exports = ModelClass;
