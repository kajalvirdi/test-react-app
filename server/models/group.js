/*
  #     #  ####  #    # ###### #      # #    # ######
  #     # #    # ##  ## #      #      # #   #  #
  ####### #    # # ## # #####  #      # ####   #####
  #     # #    # #    # #      #      # #  #   #
  #     #  ####  #    # ###### ###### # #    # ######

  Name:-> Group Model
  Description:-> This model is used  to store groups
  Author:-> Kajal
  Created On:-> 19-08-2017
-----------------------------------------------------------------------------*/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// model definition
const groupSchema = new Schema({
  title: String
});

// Create the model class
const ModelClass = mongoose.model('group', groupSchema);

// Export the model
module.exports = ModelClass;
