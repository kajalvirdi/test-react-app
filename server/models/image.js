/*
  #     #  ####  #    # ###### #      # #    # ######
  #     # #    # ##  ## #      #      # #   #  #
  ####### #    # # ## # #####  #      # ####   #####
  #     # #    # #    # #      #      # #  #   #
  #     #  ####  #    # ###### ###### # #    # ######

  Name:-> Image Model
  Description:-> This model is used  to store images sent in messages
  Author:-> Kajal
  Created On:-> 19-08-2017
-----------------------------------------------------------------------------*/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// model definition
const imageSchema = new Schema({
  img: { data: Buffer,
    contentType: String
  }
});

// Create the model class
const ModelClass = mongoose.model('image', imageSchema);

// Export the model
module.exports = ModelClass;
