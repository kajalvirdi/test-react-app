/*
  #     #  ####  #    # ###### #      # #    # ######
  #     # #    # ##  ## #      #      # #   #  #
  ####### #    # # ## # #####  #      # ####   #####
  #     # #    # #    # #      #      # #  #   #
  #     #  ####  #    # ###### ###### # #    # ######

  Name:-> Private Message Model
  Description:-> This model is used  to store private messages
  Author:-> Kajal
  Created On:-> 19-08-2017
-----------------------------------------------------------------------------*/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// model definition
const privateMessageSchema = new Schema({
  user_from: String,
  user_to: String,
  content: String,
  image: String,
  sent_at: { type: Date, default: Date.now }
});

// Create the model class
const ModelClass = mongoose.model('privateMessage', privateMessageSchema);

// Export the model
module.exports = ModelClass;
