/*
  #     #  ####  #    # ###### #      # #    # ######
  #     # #    # ##  ## #      #      # #   #  #
  ####### #    # # ## # #####  #      # ####   #####
  #     # #    # #    # #      #      # #  #   #
  #     #  ####  #    # ###### ###### # #    # ######

  Name:-> Group Message Model
  Description:-> This model is used  to store group messages
  Author:-> Kajal
  Created On:-> 19-08-2017
-----------------------------------------------------------------------------*/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// model definition
const groupMessageSchema = new Schema({
  user: String,
  content: String,
  group: String,
  image: String,
  sent_at: { type: Date, default: Date.now }
});

// Create the model class
const ModelClass = mongoose.model('groupMessage', groupMessageSchema);

// Export the model
module.exports = ModelClass;
