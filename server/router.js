/*
  #     #  ####  #    # ###### #      # #    # ######
  #     # #    # ##  ## #      #      # #   #  #
  ####### #    # # ## # #####  #      # ####   #####
  #     # #    # #    # #      #      # #  #   #
  #     #  ####  #    # ###### ###### # #    # ######

  Name:-> Route Handlers
  Description:-> This is used for managing the routing of the server.
  Author:-> Kajal
  Created On:-> 18-08-2017
-----------------------------------------------------------------------------*/
//Authentication route handlers
const Authentication = require('./controllers/authentication');

//Passport middleware module and setup
const passport = require('passport');
const passportStrategies = require('./services/passport_strategies');
const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false });

//Custom express routing middleware that checks to see if the authenticated user is an admin
const requireAdmin = require('./services/requireAdmin')


module.exports = function(app) {

  /*--------------- Open/Not-Protected Routes --------------------------------*/
  // using requireSignin passport middleware to authenticate for protected route using local (email/password) strategy)
  // Authentication.signin sends back JWT token to authenticated user
  app.post('/signin', requireSignin, Authentication.signin);

  // route for signing up user
  app.post('/signup', Authentication.signup);


  /*--------------- Auth Protested Routes ------------------------------------*/
  // using requireAuth passport middleware w/ jwt strategy to protect route
  app.get('/dashboard', requireAuth, function(req, res) {
    // res.send({ message: 'server response:  this GET request has been authorized for a user' });
    res.send({ message: "123" });
  });

  // route to get user data via user id
  app.get('/get_user/:id', requireAuth, Authentication.get_user);


}
