/*
  #     #  ####  #    # ###### #      # #    # ######
  #     # #    # ##  ## #      #      # #   #  #
  ####### #    # # ## # #####  #      # ####   #####
  #     # #    # #    # #      #      # #  #   #
  #     #  ####  #    # ###### ###### # #    # ######

  Name:-> Config
  Description:-> This config file is used to store the secret signing key
	 consisting of a random character string to generate JWTs for passport strategy
  Author:-> Kajal
  Created On:-> 18-08-2017
-----------------------------------------------------------------------------*/
module.exports = {
	secret: 'homelikesecretkey'
};
